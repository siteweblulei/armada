<!DOCTYPE html>
<html lang="en">
<?php
include 'connect.inc.php';
$conn = connectMySQL();

try {

    $sql = "SELECT shipID,shipName,uniqName,country,flagPhoto FROM Ship";
    $result = $conn->prepare($sql);
    $result->execute();

    $path = "ships_photo/";
    $flagPath = "flags/";
} catch (PDOException $e) {
    echo "Erreur !: " . $e->getMessage();
}
?>


<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ships Armada 2019</title>
<link rel="stylesheet" type="text/css" href="ships.css" />
<link rel="stylesheet" type="text/css" href="general.css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
</head>

<body style="background-color: #e3f1ff;
	height: 100%;">

	<h1 style="text-align: center; color: #174867; padding: 20px;">Invited
		ships in Armada 2019</h1>

	<ul class="nav">

		<li class="nav-item">

			<div class="dropdown">
				<button class="btn btn-primary dropdown-toggle"
					style="margin: 0.7rem" type="button" id="dropdownMenu2"
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<button class="dropdown-item" type="button"
						onclick="window.location.href='index.php'">Home</button>
					<button class="dropdown-item" type="button"
						onclick="window.location.href='ships.php'">Ships</button>
					<?php

    if (! isset($_SESSION["admin"]) || $_SESSION["admin"] === false)
        echo '<button class="dropdown-item" type="button" onclick="window.location.href=\'login.php\'">Login</button>';
    else {
        echo '<button class="dropdown-item" type="button" onclick="window.location.href=\'logout.php\'">Logout</button>';
        if ($_SESSION['authority'] === "Administrator") {
            echo '<button class="dropdown-item" type="button" onclick="window.location.href=\'permission_change.php\'">Permission Change</button>';
        }
    }

    ?>
				</div>
			</div>
		</li>
		<?php
$string = "<li class=&quot;nav-item&quot;>
					 
					  </li>";

$string2 = "<li class=&quot;nav-item&quot;>
					  <button class=&quot;btn btn-primary&quot; style=&quot;margin: 0.7rem&quot; type=&quot;button&quot;
					  onclick=&quot;window.location.href=&#39;ship_add.php&#39;&quot;>Add a new ship</button>
					  </li>";

if (! isset($_SESSION["admin"]) || $_SESSION["admin"] === false)
    echo html_entity_decode($string, ENT_QUOTES, 'UTF-8');

else {
    if ($_SESSION['authority'] === "Manager") {
        echo html_entity_decode($string2, ENT_QUOTES, 'UTF-8');
    }
}

?>

		
	</ul>


	<div class="shipbox">

	<?php

while ($row = $result->fetch()) {

    ?> 
   
		<div class="ship container-fluid" style="width: 25%; height: 50%">

			<div class="row" style="width: 100%;">
				<div class="col-8">
					<p style="color: #2c86c2; text-align: center;"><?php echo $row['shipName']; ?> </p>
				</div>
				<div class="col-4">
		
			<?php  echo "<img style='width: 40px; height: 30px; float: right' src=$flagPath".$row['flagPhoto'].">";?>
			
			
					</div>
			</div>
			<div class="ship-img row">

				<?php  echo "<img src=$path".$row['uniqName'].">";?>
				<div class="ship-button">

					<ul class="nav">
<?php
    if (! isset($_SESSION["admin"]) || $_SESSION["admin"] === false) {
        echo '<p style="color:#3f98d3; margin: 0 auto;">Login to discover more</p>';
        echo '<button class="btn btn-secondary" style="margin: 0.3rem"
        type="button" disabled>Details</button>';
    } elseif ($_SESSION['authority'] === "Manager") {
        ?>
						<li class="nav-item">


							<form action="ship_details.php" method="post">

								<input type='hidden' name='detail'
									value='<?php echo $row['shipID']?>'>

								<button id="button1" class="btn btn-info" style="margin: 0.3rem"
									type="submit" onclick="window.location.href='ship_details.php'">Details</button>

							</form>
						</li>

						<li class="nav-item">

							<form action="ship_modify.php" method="post">
								<input type='hidden' name='modify'
									value='<?php echo $row['shipID']?>'>

								<button id="button2" class="btn btn-primary"
									style="margin: 0.3rem" type="submit"
									onclick="window.location.href='ship_modify.php'">modify</button>

							</form>
						</li>
						<li class="nav-item">

							<form action="ship_delete.php" method="post">
								<input type='hidden' name='delete'
									value='<?php echo $row['shipID']?>'>

								<button id="button3" class="btn btn-warning"
									style="margin: 0.3rem"
									onclick="window.location.href='ship_delete.php'">delete</button>
							</form>
								
						</li>
						<?php }elseif ($_SESSION['authority'] === "Visitor") {?>
						
						    <li class="nav-item">


							<form action="ship_details.php" method="post">

								<input type='hidden' name='detail'
									value='<?php echo $row['shipID']?>'>

								<button id="button1" class="btn btn-info" style="margin: 0.3rem"
									type="submit" onclick="window.location.href='ship_details.php'">Details</button>

							</form>
						</li>
					<?php  }  ?>
					</ul>
				</div>
			</div>
		</div>
		<?php
}
?>
	</div>


</body>
</html>
