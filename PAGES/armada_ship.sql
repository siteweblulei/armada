CREATE TABLE Ship(
  

  shipID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   

 shipName VARCHAR(30) NOT NULL,
    
crew INT(255) NOT NULL,
  
  typeShip VARCHAR(30) NOT NULL,
 
launchYear INT(255) NOT NULL,
  
length INT(255) NOT NULL,
 

country VARCHAR(30) NOT NULL,
 
flagPhoto VARCHAR(50) NOT NULL,
arrivalDate DATE,
dptDate DATE,
 presentation VARCHAR(150),
  
 uniqName VARCHAR(150)
);