Create Table User

(
UserID int NOT NULL AUTO_INCREMENT,

PRIMARY KEY(UserID),

Username varchar(30) NOT NULL,

Password varchar(300) NOT NULL,

FirstName varchar(30),

LastName varchar(30),

Gender varchar(10),
Authority varchar(30)
);
