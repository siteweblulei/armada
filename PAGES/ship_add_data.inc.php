<?php
include 'connect.inc.php';

$conn = connectMySQL();

$shipName = $_POST['shipName'];
$crew = $_POST['crew'];
$typeShip = $_POST['typeShip'];
$launchYear = $_POST['launchYear'];
$length = $_POST['length'];
$country = $_POST['country'];
$arrivalDate = $_POST['arrivalDate'];
$dptDate = $_POST['dptDate'];
$presentation = $_POST['presentation'];

$photo_name = $_FILES['shipPhoto']['name'];

$photo_type = $_FILES['shipPhoto']['type'];
$uploadPath = 'ships_photo/';
$allowedExts = array(
    "gif",
    "jpeg",
    "jpg",
    "png"
);
$temp = explode(".", $photo_name);

$extension = end($temp);

 try {
     
     $sql = "SELECT * FROM Ship where shipName = :shipName";
     $result = $conn->prepare($sql);
     $result ->bindParam(':shipName', $shipName);
     $result ->execute();
     
 } catch (PDOException $e) {
     echo "Erreur !: " . $e->getMessage();
 }
 
if (! preg_match('/^\w+$/i', $shipName)) {
    echo "<script>alert('Do not use illegal characters.');history.go(-1);</script>";
} elseif (($crew) > 500) {
    echo "<script>alert('crew members are too much.');history.go(-1);</script>";
} elseif(!is_numeric($crew)){
    echo "<script>alert('crew members should be integer.');history.go(-1);</script>";
    
}elseif ((($launchYear) < 1500) || (($launchYear) > 2018)) {
    echo "<script>alert('Launch year invalid.');history.go(-1)</script>";
}elseif(!is_numeric($launchYear)){
        echo "<script>alert('launch year should be integer.');history.go(-1);</script>";
        
}elseif((($length)<1) || (($length)>100)){
    echo "<script>alert('Length invalid.');history.go(-1)</script>";
}elseif(!is_numeric($length)){
    echo "<script>alert('length should be integer.');history.go(-1);</script>";
}elseif(strlen($presentation)>150){
    echo "<script>alert('Please compress the presentation into 150 words.');history.go(-1)</script>";
}elseif ($result->rowCount() == 1) {
    echo "<script>alert('Your ship exist already.');history.go(-1)</script>";
}elseif ((($photo_type == "image/gif") || ($photo_type == "image/jpeg") || ($photo_type == "image/jpg") || ($photo_type == "image/pjpeg") || ($photo_type == "image/x-png") || ($photo_type == "image/png")) && ($_FILES["shipPhoto"]["size"] < 1048576) && in_array($extension, $allowedExts)) {
    if ($_FILES["shipPhoto"]["error"] > 0) {
        echo "<script>alert('Photo is not allowed.');history.go(-1)</script>";
    } else {

        $uniq = uniqid();
        $uniqName = $uniq . strrchr($_FILES['shipPhoto']['name'], ".");

        if (file_exists($uploadPath . $uniqName)) {
            echo "<script>alert('Please wait a second.');history.go(-1)</script>";
        } else {

            connectMySQL();

            try {
                $flagPhoto =$country.'.jpg';
                $sql = "INSERT INTO Ship (shipName,crew,typeShip,launchYear,length,country,flagPhoto,arrivalDate,dptDate,presentation,uniqName) VALUES (:shipName,:crew,:typeShip,:launchYear,:length,:country,:flagPhoto,:arrivalDate,:dptDate,:presentation,:uniqName)";
                $result2 = $conn->prepare($sql);
                $result2 ->bindParam(':shipName', $shipName);
                $result2 ->bindParam(':crew', $crew);
                $result2 ->bindParam(':typeShip', $typeShip);
                $result2 ->bindParam(':launchYear', $launchYear);
                $result2 ->bindParam(':length', $length);
                $result2 ->bindParam(':country', $country);
                $result2 ->bindParam(':flagPhoto',$flagPhoto);
                $result2 ->bindParam(':arrivalDate', $arrivalDate);
                $result2 ->bindParam(':dptDate', $dptDate);
                $result2 ->bindParam(':presentation', $presentation);
                $result2 ->bindParam(':uniqName', $uniqName);
                
                $result2 ->execute();

                $upload = move_uploaded_file($_FILES["shipPhoto"]["tmp_name"], $uploadPath . $uniqName);
                if (($upload)&&($result2->rowCount()!=0)) {

                    echo "<script>alert('added ship successfully!');window.location.href='ships.php'</script>";
                }
		else{
			echo "<script>alert('upload error'); history.go(-1)</script>";
}
            } catch (PDOException $e) {
                echo "Erreur !: " . $e->getMessage();
            }
        }
    }
} else {
    echo "<script>alert('Invalid type.');history.go(-1)</script>";
}
?>




 