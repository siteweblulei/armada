<?php
include 'connect.inc.php';
$conn = connectMySQL();

$shipID = $_POST['modify_id'];
$crew = $_POST['crew'];
$typeShip = $_POST['typeShip'];
$launchYear = $_POST['launchYear'];
$length = $_POST['length'];
$country = $_POST['country'];
$arrivalDate = $_POST['arrivalDate'];
$dptDate = $_POST['dptDate'];
$presentation = $_POST['presentation'];


if (($crew) > 500) {
    echo "<script>alert('crew members are too much.');history.go(-1)</script>";
} else
    if(!is_numeric($crew)){
        echo "<script>alert('crew members should be integer.');history.go(-1);</script>";
        
}elseif ((($launchYear) < 1500) || (($launchYear) > 2018)) {
    echo "<script>alert('Launch year invalid.');history.go(-1)</script>";
}else
    if(!is_numeric($launchYear)){
        echo "<script>alert('launch year should be a number.');history.go(-1);</script>";
        
}elseif((($length)<1) || (($length)>100)){
    echo "<script>alert('Length invalid.');history.go(-1)</script>";
}elseif(!is_numeric($length)){
    echo "<script>alert('length should be a number.');history.go(-1);</script>";
    
}elseif(strlen($presentation)>150){
    echo "<script>alert('Please compress the presentation into 150 words.');history.go(-1)</script>";
}else
{
            connectMySQL();

            try {
                $flagPhoto =$country.'.jpg';
                $sql = "UPDATE Ship SET crew=:crew,typeShip=:typeShip,launchYear=:launchYear,length=:length,country=:country,flagPhoto=:flagPhoto,arrivalDate=:arrivalDate,dptDate=:dptDate,presentation=:presentation WHERE shipID=:shipID";
              $result= $conn->prepare($sql);

              $result ->bindParam(':crew', $crew);
              $result ->bindParam(':typeShip', $typeShip);
              $result ->bindParam(':launchYear', $launchYear);
              $result ->bindParam(':length', $length);
              $result ->bindParam(':country', $country);
              $result ->bindParam(':flagPhoto',$flagPhoto);
              $result ->bindParam(':arrivalDate', $arrivalDate);
              $result ->bindParam(':dptDate', $dptDate);
              $result ->bindParam(':presentation', $presentation);
              $result ->bindParam(':shipID', $shipID);
              $result->execute();
              
          if($result==TRUE){
              if($result->rowCount()==0){
                  echo "<script>alert('You didn\'t modify anything. ');history.go(-1)</script>";
              }else
                echo "<script>alert('modified your ship successfully!');window.location.href='ships.php'</script>";
                }
		elseif($result==FALSE){
		 echo "<script>alert('Something wrong... ');history.go(-1)</script>";
}


            } catch (PDOException $e) {
                echo "Erreur !: " . $e->getMessage();
            }
    
} 
?>




 