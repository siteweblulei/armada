<!DOCTYPE html>

<?php
session_start();
if(!isset($_SESSION["admin"]) || $_SESSION["admin"]===false)
		echo "<script>alert('You need to log in first');window.location.href='login.php'</script>";
elseif($_SESSION["authority"]=='Administateur')
		echo "<script>alert('You can\'t see this page');window.location.href='index.php'</script>";

include 'connect.inc.php';
$conn = connectMySQL();
$shipID = $_POST['detail'];

try {

    $sql = "SELECT * FROM Ship where shipID = :shipID";
    $result = $conn->prepare($sql);
    $result->bindParam(':shipID', $shipID);
    $result->execute();

    $path = "ships_photo/";
} catch (PDOException $e) {
    echo "Erreur !: " . $e->getMessage();
}
?>


<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ship Details in Armada 2019</title>
<link rel="stylesheet" type="text/css" href="general.css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

</head>
<body style="background-color: #e3f1ff; height: 1100px;">


	<h1 style="text-align: center; color: #174867; padding: 20px;">Ship's
		detail in Armada 2019</h1>



	<ul class="nav">

		<li class="nav-item">

			<div class="dropdown">
				<button class="btn btn-primary dropdown-toggle"
					style="margin: 0.7rem" type="button" id="dropdownMenu2"
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<button class="dropdown-item" type="button"
						onclick="window.location.href='index.php'">Home</button>
					<button class="dropdown-item" type="button"
						onclick="window.location.href='ships.php'">Ships</button>
					<?php 
					    
					    if(!isset($_SESSION["admin"]) || $_SESSION["admin"]===false)
					        echo '<button class="dropdown-item" type="button" onclick="window.location.href=\'login.php\'">Login</button>';																	
					    else{
					        echo '<button class="dropdown-item" type="button" onclick="window.location.href=\'logout.php\'">Logout</button>';
					        if($_SESSION['authority']==="Administrator"){
					        	echo 
					        	'<button class="dropdown-item" type="button" onclick="window.location.href=\'permission_change.php\'">Permission Change</button>';
					        }
					        
					    }

					 ?>
				</div>
			</div>
		</li>

	</ul>


	<div class="addbox">
		<div class="container-fluid">
	
	
	<?php  while ($row=$result->fetch()) { ?> 
	
	<h2 class="row justify-content-center" id="detail"><?php echo $row['shipName']?> </h2>
			<br> <br>
			<div class="row justify-content-center">
	<?php  echo "<img src=$path".$row['uniqName'].">";?>
	</div>
			<br> <br>
			<p id="detail">Crew : <?php echo $row['crew']?>  members</p>
			<p id="detail">Type : <?php echo $row['typeShip']?></p>
			<p id="detail">Launched in : <?php echo $row['launchYear']?></p>
			<p id="detail">Overall Length : <?php echo $row['length']?> m</p>
			<p id="detail">Country : <?php echo $row['country']?></p>

			<br> <br>
			<p id="detail">Something you should know about the ship:</p>
			<p style="color: #174867"><?php echo $row['presentation']?></p>
			<br> <br>

		</div>

		<form action="getPDF.php" method="post">
			<input type='hidden' name='pdf' value='<?php echo $row['shipID']?>'>
<?php
    if ($_SESSION['authority'] === "Administrator"||$_SESSION['authority'] === "Manager") {
        echo '<button type="submit" class="btn btn-primary" value="submit" style="float: right" onclick="window.location.href=\'getPDF.php\'">download in PDF</button>';

        echo '<button type="button" class="btn btn-primary"
				onclick="window.location.href=\'ships.php\'">Return</button>';
    }
    ?>

    </form>
		<form action="getPDFinscrit.php" method="post">
			<input type='hidden' name='pdf' value='<?php echo $row['shipID']?>'>
    
 <?php

if ($_SESSION['authority'] === "Visitor") {
        echo '<button type="submit" class="btn btn-primary" value="submit"
				style="float: right" onclick="window.location.href=\'getPDFinscrit.php\'">download
				in PDF</button>';
        echo '<button type="button" class="btn btn-primary"
				onclick="window.location.href=\'ships.php\'">Return</button>';
    }
    ?>
			<br> <br>
		</form>
	<?php }?>
	</div>




</body>
</html>
