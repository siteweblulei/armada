<!DOCTYPE html>

<?php 
session_start();
if(!isset($_SESSION["admin"]) || $_SESSION["admin"]===false)
		echo "<script>alert('You need to log in first');window.location.href='login.php'</script>";
	elseif($_SESSION["authority"]!='Manager')
		echo "<script>alert('You can\'t see this page');window.location.href='index.php'</script>";
include 'connect.inc.php';
$conn = connectMySQL();
$shipID=$_POST['modify'];

try{
 
    
    $sql = "SELECT * FROM Ship where shipID = :shipID";
    $result = $conn->prepare($sql);
    $result ->bindParam(':shipID', $shipID);
    $result ->execute();
    
} catch (PDOException $e) {
    echo "Erreur !: " . $e->getMessage();
}
?>


<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Modify a Ship Armada 2019</title>
<link rel="stylesheet" type="text/css" href="general.css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

</head>
<body style="background-color: #e3f1ff; height: 1200px;">


	<h1 style="text-align: center; color: #174867; padding: 20px;">Modify a
		ship in Armada 2019</h1>



	<ul class="nav">

		<li class="nav-item">

			<div class="dropdown">
				<button class="btn btn-primary dropdown-toggle"
					style="margin: 0.7rem" type="button" id="dropdownMenu2"
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<button class="dropdown-item" type="button"
						onclick="window.location.href='index.php'">Home</button>
					<button class="dropdown-item" type="button"
						onclick="window.location.href='ships.php'">Ships</button>
					<?php 
					    
					    if(!isset($_SESSION["admin"]) || $_SESSION["admin"]===false)
					        echo '<button class="dropdown-item" type="button" onclick="window.location.href=\'login.php\'">Login</button>';																	
					    else{
					        echo '<button class="dropdown-item" type="button" onclick="window.location.href=\'logout.php\'">Logout</button>';
					        if($_SESSION['authority']==="Administrator"){
					        	echo 
					        	'<button class="dropdown-item" type="button" onclick="window.location.href=\'permission_change.php\'">Permission Change</button>';
					        }
					        
					    }

					 ?>
				</div>
			</div>
		</li>

	</ul>


	<div class="addbox">
	<?php  while ($row=$result->fetch()) { ?> 
	
		<form action="ship_modify_data.inc.php" method="POST"
			enctype="multipart/form-data">

			<h2 class="justify-content-center" id="detail"><?php echo $row['shipName']?> </h2>

			<div class="form-group">
				<label for="crew">Number of crew members</label><span
					class="required">*</span> <input class="form-control" type="text"
					name="crew" value="<?php echo $row['crew']?>"
					required="required" />
			</div>

			<div class="form-group">
				<label for="typeShip">type of ship</label><span class="required">*</span>
				<select class="form-control" name="typeShip">
					<option value="<?php echo $row['typeShip']?>">Original type</option>
					<option value="Three-masted barque">Three-masted barque</option>
				<option value="Warship">Warship</option>
				<option value="Three-masted schooner">Three-masted schooner</option>
				<option value="Galleon">Galleon</option>
				<option value="Four-masted ship">Four-masted ship</option>
				</select>

			</div>
			<div class="form-group">
				<label for="launchYear">Launch in </label> <span class="required">*</span>
				<input class="form-control" type="text" name="launchYear"
					value="<?php echo $row['launchYear']?>" required="required" />
			</div>
			<div class="form-group">
				<label for="length">Overall length(meters) </label> <span
					class="required">*</span> <input class="form-control" type="text"
					name="length" value="<?php echo $row['length']?>" required="required" />
			</div>
			<div class="form-group">
				<label for="country">Country </label> <span class="required">*</span>
				<select class="form-control" name="country">
				<option value="<?php echo $row['country']?>">Original country</option>
					<option value="France">France</option>
				<option value="England">England</option>
				<option value="Netherlands">Netherlands</option>
				<option value="Spain">Spain</option>
				</select>

			</div>
			
				<div class="form-group">
				<label for="arrivalDate">Arrival date</label><span class="required">*</span>
				<input type="date" name="arrivalDate" value="<?php echo $row['arrivalDate']?>" required="required" />
			</div>
			
			<div class="form-group">
				<label for="">Departure date</label><span class="required">*</span>
				<input type="date" name="dptDate" value="<?php echo $row['dptDate']?>" required="required" />
			</div>

			<div class="form-group">
				<label for="presentation">Some presentation </label><span
					class="required">*</span>
				<input class="form-control" name="presentation"
					value="<?php echo $row['presentation']?>" required="required">
			</div>
		
			<br>
				
				<input type='hidden' name="modify_id" value='<?php echo $row['shipID']?>'/>
				
			<button type="submit" class="btn btn-primary" style="float: right"
				value="submit">Modify</button>
			<button type="button" class="btn btn-primary"
				onclick="window.location.href='ships.php'">Return</button>
			<br>
			<br>
			<br>
			<br>
	</form>
	</div>


	
	<?php }?>


</body>
</html>