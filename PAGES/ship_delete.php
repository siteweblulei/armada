<!DOCTYPE html>

<?php
session_start();
if(!isset($_SESSION["admin"]) || $_SESSION["admin"]===false)
		echo "<script>alert('You need to log in first');window.location.href='login.php'</script>";
	elseif($_SESSION["authority"]!='Manager')
		echo "<script>alert('You can\'t see this page');window.location.href='index.php'</script>";
include 'connect.inc.php';
$conn = connectMySQL();
$shipID=$_POST['delete'];

try{
 
    
    $sql = "SELECT * FROM Ship where shipID = :shipID";
    $result = $conn->prepare($sql);
    $result ->bindParam(':shipID', $shipID);
    $result ->execute();
    $row=$result->fetch();
    
} catch (PDOException $e) {
    echo "Erreur !: " . $e->getMessage();
}
?>

<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Delete a ship Armada 2019</title>
<link rel="stylesheet" type="text/css" href="general.css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>

</head>
<body style="background-color: #e3f1ff; height: 100%;">


	<h1 style="text-align: center; color: #174867; padding: 20px;">Delete a
		ship in Armada 2019</h1>



	<ul class="nav">

		<li class="nav-item">

			<div class="dropdown">
				<button class="btn btn-primary dropdown-toggle"
					style="margin: 0.7rem" type="button" id="dropdownMenu2"
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<button class="dropdown-item" type="button" onclick="window.location.href='index.php'">Home</button>
					<button class="dropdown-item" type="button" onclick="window.location.href='ships.php'">Ships</button>
					<?php 
					    
					    if(!isset($_SESSION["admin"]) || $_SESSION["admin"]===false)
					        echo '<button class="dropdown-item" type="button" onclick="window.location.href=\'login.php\'">Login</button>';																	
					    else{
					        echo '<button class="dropdown-item" type="button" onclick="window.location.href=\'logout.php\'">Logout</button>';
					        if($_SESSION['authority']==="Administrator"){
					        	echo 
					        	'<button class="dropdown-item" type="button" onclick="window.location.href=\'permission_change.php\'">Permission Change</button>';
					        }
					        
					    }

					 ?>

				</div>
			</div>
		</li>

	</ul>


	<div class="addbox" style="height: 50%">
		<form action="ship_delete_data.inc.php" method="POST" enctype="multipart/form-data">
		<div class="container-fluid">
			<div class="row justify-content-center">
			<h5>Are you sure you want to delete all the information of 
			<b><?php echo $row['shipName']?> </b> ?</h5>
			</div>
			</div>
			<br><br>
			<input type='hidden' name="delete_id" value='<?php echo $row['shipID']?>'/>
			
<button type="submit" class="btn btn-danger" style="float:right" value="submit">Delete</button>
			<button type="button" class="btn btn-primary"
				onclick="window.location.href='ships.php'">Return</button>
				<br><br>

	</form>
	</div>



</body>
</html>
