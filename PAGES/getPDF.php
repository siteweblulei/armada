<?php

include 'connect.inc.php';
$conn = connectMySQL();
$shipID=$_POST['pdf'];
try{
 
    
    $sql = "SELECT * FROM Ship where shipID = :shipID";
    $result = $conn->prepare($sql);
    $result ->bindParam(':shipID', $shipID);
    $result ->execute();
     $path="ships_photo/";
    
} catch (PDOException $e) {
    echo "Erreur !: " . $e->getMessage();
}


require('./fpdf/fpdf.php');
  

  while ($row=$result->fetch()) {
    $shipname=$row['shipName'];
  $img=$path.$row['uniqName'];
  $crew=$row['crew'];
  $typeShip=$row['typeShip'];
  $launchYear=$row['launchYear'];
  $length=$row['length'];
  $country=$row['country'];
  $arrivalDate=$row['arrivalDate'];
  $dptDate=$row['dptDate'];
  $presentation=$row['presentation'];
  
  }

          class PDF extends FPDF{

        function shipPDF($shipname,$img,$crew,$typeShip,$launchYear,$length,$country,$arrivalDate,$dptDate,$presentation){
              $this->setFont('Arial','B','36');
               $this->setX('80');
            $this->cell(40,20,$shipname,15);
            $this->ln();
        
            
        $this->setX('50');
            $this->image($img,null,null,90,65);
            $this->ln(5);
          
            $this->setFont('Arial','B','14');
            $this->setX('25');
            $this->cell(10,10,'crew members:  ');
            $this->setFont('Arial','','14');
            $this->setX('80');
            $this->cell(10,10,$crew);
            $this->ln();
         
            $this->setFont('Arial','B','14');
            $this->setX('25');
            $this->cell(10,10,'ship\'s type:  ');
            $this->setFont('Arial','','14');
            $this->setX('80');
            $this->cell(10,10,$typeShip);
            $this->ln();
            
            $this->setFont('Arial','B','14');
            $this->setX('25');
            $this->cell(10,10,'launched in:  ');
            $this->setFont('Arial','','14');
            $this->setX('80');
            $this->cell(10,10,$launchYear);
            $this->ln();
            
            $this->setFont('Arial','B','14');
            $this->setX('25');
            $this->cell(10,10,'overall length:  ');
            $this->setFont('Arial','','14');
            $this->setX('80');
            $this->cell(10,10,$length);
            $this->ln();
            
            $this->setFont('Arial','B','14');
            $this->setX('25');
            $this->cell(10,10,'country:  ');
            $this->setFont('Arial','','14');
            $this->setX('80');
            $this->cell(10,10,$country);
            $this->ln();
            
            $this->setFont('Arial','B','14');
            $this->setX('25');
            $this->cell(10,10,'Arrival date:  ');
            $this->setFont('Arial','','14');
            $this->setX('80');
            $this->cell(10,10,$arrivalDate);
            $this->ln();
            
            $this->setFont('Arial','B','14');
            $this->setX('25');
            $this->cell(10,10,'Departure date:  ');
            $this->setFont('Arial','','14');
            $this->setX('80');
            $this->cell(10,10,$dptDate);
            $this->ln();
            
            $this->setFont('Arial','B','14');
            $this->setX('25');
            $this->cell(10,10,'something you should know about the ship:  ');
            $this->ln();
            $this->setFont('Arial','','14');
            $this->setX('30');
            $this->write(10,$presentation);
            $this->ln();
        }
    }


$pdf=new PDF();
    $pdf->AddPage();
   
   
    $pdf->shipPDF($shipname,$img,$crew,$typeShip,$launchYear,$length,$country,$arrivalDate,$dptDate,$presentation);
         
    $pdf->output();
?>

