<!DOCTYPE html>
<html lang="en" style="height:100%;">
<head>
<?php session_start();
?>
<meta charset="UTF-8">
<title>Armada 2019</title>

<link rel="stylesheet" type="text/css" href="general.css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
	

<script>
    window.onbeforeunload = function b(){
 	window.location.href="logout.php";
};
 
</script>


</head>
<body style="background-color: #e3f1ff;margin: 0;
padding: 0;
min-height: 100%;
position: relative;">


	<h1 style="text-align: center; color: #174867; padding: 20px;">Welcome
		to Armada 2019!</h1>


<div class="container-fluid">
<div class="row">
	<ul class="col-3 nav">

		<li class="nav-item">

			<div class="dropdown">
				<button class="btn btn-primary dropdown-toggle"
					style="margin: 0.7rem" type="button" id="dropdownMenu2"
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<button class="dropdown-item" type="button"
						onclick="window.location.href='index.php'">Home</button>
					<button class="dropdown-item" type="button"
						onclick="window.location.href='ships.php'">Ships</button>
					<?php 
					    
					    if(!isset($_SESSION["admin"]) || $_SESSION["admin"]===false)
					        echo '<button class="dropdown-item" type="button" onclick="window.location.href=\'login.php\'">Login</button>';																	
					    else{
					        echo '<button class="dropdown-item" type="button" onclick="window.location.href=\'logout.php\'">Logout</button>';
					        if($_SESSION['authority']==="Administrator"){
					        	echo 
					        	'<button class="dropdown-item" type="button" onclick="window.location.href=\'permission_change.php\'">Permission Change</button>';
					        }
					        
					    }

					 ?>
					 

				</div>
			</div>
		</li>

	</ul>
<div class="col-9 justify-content-center">
<img style="width: 70%; min-width: 250px; box-shadow: 0px 15px 15px rgba(0, 0, 0, 0.4);"
	alt=""	src="welcome.jpg">
	</div>
	</div>
	</div>
  
    <footer>
   2018 © Armada - grp_6_16 | Designed by Xintong LU, Yuxiao LEI</footer>


</body>

</html>
